import React from 'react';
import {LoginBox} from 'acme-login-box';
import Coyote from 'coyote'

const onLogin = function(credentials){
  const isLogin = Coyote.services
                        .user
                        .login
                        .execute(credentials);
  alert( isLogin ? 'Estas dentro' : 'Fallo!!!' );

}

React.render(<LoginBox onLogin={onLogin}/>, document.getElementById('login-container'));
